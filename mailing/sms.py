import json
from datetime import timedelta

from requests.auth import AuthBase
import requests

from config.environ import env
from loguru import logger
from huey.contrib.djhuey import task


class JWTAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + self.token
        return r


@task(retries=3, retry_delay=timedelta(minutes=5))
def send_message(message_id: int, phone_number: str, message_text: str) -> None:
    jwt_token = env("JWT_TOKEN")
    session = requests.Session()
    session.auth = JWTAuth(jwt_token)
    url = f"https://probe.fbrq.cloud/v1/send/{message_id}"

    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    data = {"id": message_id, "phone": phone_number, "text": message_text}

    try:
        response = session.post(url, headers=headers, data=json.dumps(data))
        response.raise_for_status()
        logger.info(f"Message text send with this status code {response.status_code}")
        return None
    except requests.exceptions.HTTPError as e:
        logger.error(f"Message not sent with this error: {e}")
        return None
    except Exception as e:
        logger.error(f"Error system: {e}")
        return None
