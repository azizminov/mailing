import uuid

from django.db import models


class Mailing(models.Model):
    class MobileOperator(models.IntegerChoices):
        MTS = 91
        BEELINE = 99
        MEGAFON = 92
        TELE2 = 97
        YOTA = 999

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    start_time = models.DateTimeField()
    message_text = models.TextField()
    end_time = models.DateTimeField()
    mobile_operator_code = models.IntegerField(choices=MobileOperator.choices)
    tag = models.CharField(max_length=100, blank=True)


class Client(models.Model):
    class MobileOperator(models.IntegerChoices):
        MTS = 91
        BEELINE = 99
        MEGAFON = 92
        TELE2 = 97
        YOTA = 999

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    phone_number = models.CharField(max_length=12)
    mobile_operator_code = models.IntegerField(choices=MobileOperator.choices)
    tag = models.CharField(max_length=100, blank=True)
    timezone = models.CharField(max_length=100, blank=True)


class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    creation_time = models.DateTimeField(auto_now_add=True)
    send_status = models.CharField(max_length=20)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
