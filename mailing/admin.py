from django.contrib import admin

from .models import Mailing, Message, Client


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class MessageAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class ClientAdmin(admin.ModelAdmin):
    pass
