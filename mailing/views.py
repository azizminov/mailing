from rest_framework import viewsets
from .models import Mailing, Client, Message
from mailing.serializers import MailingSerializer, ClientSerializer, MessageSerializer
from django.db.models import Q

from mailing.services.mailing import send_message_for_all_picked_clients


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def perform_create(self, serializer):
        mailing = serializer.save()
        clients = Client.objects.filter(
            Q(mobile_operator_code=mailing.mobile_operator_code) | Q(tag=mailing.tag)
        )
        send_message_for_all_picked_clients(mailing, clients)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
