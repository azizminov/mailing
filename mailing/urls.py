from django.urls import path, include
from rest_framework import routers
from .views import MailingViewSet, ClientViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register("mailings", MailingViewSet)
router.register("clients", ClientViewSet)
router.register("messages", MessageViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
