all:
	@echo "make up			- Create & setup development"
	@echo "make lint		- Format code"
	@echo "make exec		- Connect web container"
	@echo "make migrations		- Create migrations files"
	@echo "make migrate		- Migrate migrations files"
	@echo "make restart		- Restart web container"
	@echo "make static		- Collect static"
	@exit 0
up:
	docker-compose up -d --no-deps --build
logs:
	docker logs --follow --tail 20 --timestamps mailing_web
exec:
	docker exec -it mailing_web bash
migrations:
	docker exec -it mailing_web python manage.py makemigrations
migrate:
	docker exec -it mailing_web python manage.py migrate
restart:
	docker restart mailing_web
lint:
	black mailing/ && flake8 --ignore=E501 --exclude=venv,docs .
static:
	docker exec -it mailing_web python manage.py collectstatic