# Mailing Test

Technology stack: Python3, Django, DRF, PostgresSQL, Redis, Huey(task-manager)
Deploy and UP: Docker and Docker Compose

## Table of Contents

* [Setup](#setup)
* [Documentation](#documentation)
* [Linting & Formatting](#linting-and-formatting)
* [Solve problems with Docker on M1](#solve-problems-with-docker-on-m1)
* [Optional Resolved Tasks](#optional-resolved-tasks)

## Setup

Create .env and .db.env files.
Set the environment variables. 
Use this command:
```bash
cp .env.example .env
```
### With Docker



Install [docker](https://www.docker.com/get-started) and [docker-compose](https://docs.docker.com/compose/)

After installation run the following command
```bash
make up
```
Or
```bash
docker-compose up -d --no-deps --build
```

And you are good to go


### Without Docker

*The following setup is for Linux users only*

Create virtual environment

```bash
python3 -m venv venv
```

Activate it

```bash
source venv/bin/activate
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.

```bash
pip3 install -r requirements.txt
```


Use `migrate` command to create tables and set relationships in your database.

```bash
python3 manage.py migrate
```

or

```bash
django-admin migrate
```

Run
```bash
python3 manage.py runserver
```
And you are good to go

## Documentation

* Link to the [docs](http://127.0.0.1:8000/docs/)
## Linting and Formatting

Linting completed with flake8
```bash
flake8 --ignore=E501 --exclude=venv,docs .
```

Formatting completed with black
```bash
black shortener/
```

## Solve problems with Docker on M1

```bash
export DOCKER_DEFAULT_PLATFORM=linux/amd64
```

## Optional Resolved tasks
* Опциональные пункты, выполнение любого количества из приведённого списка повышают ваши шансы на положительное решение о приёме
* подготовить docker-compose для запуска всех сервисов проекта одной командой
* сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
* реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
* удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
* написать конфигурационные файлы (deployment, ingress, …) для запуска проекта в kubernetes и описать как их применить к работающему кластеру