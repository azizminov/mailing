from datetime import timedelta

from huey.contrib.djhuey import task
from loguru import logger
from mailing.models import Message
from mailing.sms import send_message


@task(retries=3, retry_delay=timedelta(minutes=5))
def send_message_for_all_picked_clients(mailing, clients):
    for client in clients:
        try:
            send_message.schedule(
                args=(
                    123,
                    client.phone_number,
                    mailing.message_text,
                ),
                eta=mailing.start_time,
                expires=mailing.end_time,
            )
            message = Message(mailing=mailing, client=client, send_status="SENT")
        except Exception as e:
            message = Message(mailing=mailing, client=client, send_status="FAILED")
            logger.error(f"{e}")
        message.save()
